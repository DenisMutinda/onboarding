const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const institutionSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    courses: {
      type: Array,
      required: false,
    },
  },
  { timestamps: true }
);

const Institution = mongoose.model("Institution", institutionSchema);

module.exports = Institution;
