const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require('body-parser');

// Schemas.
const Institution = require("./models/institution");
const Course = require("./models/course");
const Student = require("./models/student");

// Instance of Express App.
const app = express();
// Needed for accessing data from the URL
app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.json()); //parse application/json

// For your eyes only.
const dbURL =
  "mongodb+srv://db_admin:Password@cluster0.5glfy.mongodb.net/on_boarding?retryWrites=true&w=majority&useNewUrlParser=true&useUnifiedTopology=true";

// Connect to db then listen on port 3k.
mongoose
  .connect(dbURL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then((result) => {
    console.log("Connected to server");
    app.listen(3000);
  })
  .catch((err) => console.log(err));

// register view engine
app.set("view engine", "ejs");
// middleware & static files
app.use(express.static("public"));

// Routes
app.get("/", (req, res) => {
  res.redirect("/institution");
});

app.get("/institution", (req, res) => {
  Institution.find()
    .then((result) => {
      res.render("index", {
        title: "Institutions",
        data: result,
      });
    })
    .catch((err) => console.log(err));
});

app.post("/institution", (req, res) => {
 
  const data = {
    name : req.body.name,
    courses : req.body.course.split(",")
  };

  const institution = new Institution(data);

  institution.save()
  .then(result => res.redirect("/institution"))
  .catch(err => console.log(err));
});

app.get("/update_institution/:id", (req, res) => {
  const id = req.params.id;

  Institution.findById(id)
  .then(result => res.render('update_institution', {title: "Update Institution", data: result}))
  .catch( err => console.log(err));
});

app.put("/institution/:id", (req, res) => {
  const id = req.params.id;

  Institution.findByIdAndUpdate(id, req.body, {useFindAndModify: false})
  .then(result => {
    res.json({redirect:"/institution"});
  })
  .catch(err => console.log(err));
});

app.delete("/institution/:id", (req, res) => {
  const id = req.params.id;
  
  Institution.findByIdAndDelete(id)
    .then(result => {
      res.json({ redirect: '/institution' });
    })
    .catch(err => {
      console.log(err);
    });
});

app.get("/course", (req, res) => {
  Course.find()
    .then((result) => {
      res.render("course", { title: "Courses", data: result });
    })
    .catch((err) => console.log(err));
});

app.post("/course", (req, res) => {
  const course = new Course(req.body);

  course.save()
  .then(result => {
    res.redirect("/course");
  })
  .catch(err => console.log(err));
});

app.get("/update_course/:id", (req, res) => {
  const id = req.params.id;

  Course.findById(id)
  .then(result => res.render('update_course', {title: "Update Course", data: result}))
  .catch( err => console.log(err));
});

app.put("/course/:id", (req, res) => {

  const id = req.params.id;

  Course.findByIdAndUpdate(id, req.body, {useFindAndModify: false})
  .then(result => {
    res.json({redirect:"/course"});
  })
  .catch(err => console.log(err));
});

// Delete Course
app.delete("/course/:id", (req, res) => {
  const id = req.params.id;
  
  Course.findByIdAndDelete(id)
    .then(result => {
      res.json({ redirect: '/course' });
    })
    .catch(err => {
      console.log(err);
    });
});

app.get("/student", (req, res) => {
  Student.find()
    .then((result) => {
      res.render("student", { title: "Students", data: result });
    })
    .catch((err) => console.log(err));
});

app.post("/student", (req, res) => {
  const student = new Student(req.body);

  student.save()
  .then(result => {
    res.redirect("/student");
  })
  .catch(err => console.log(err));
});

app.put("/student/:id", (req, res) => {

  const id = req.params.id;

  Student.findByIdAndUpdate(id, req.body, {useFindAndModify: false})
  .then(result => {
    res.json({redirect:"/student"});
  })
  .catch(err => console.log(err));
});

app.get("/update_student/:id", (req, res) => {
  const id = req.params.id;

  Student.findById(id)
  .then(result => res.render('update_student', {title: "Update Student", data: result}))
  .catch( err => console.log(err));
});

app.delete('/student/:id', (req, res) => {
  const id = req.params.id;
  
  Student.findByIdAndDelete(id)
    .then(result => {
      res.json({ redirect: '/student' });
    })
    .catch(err => {
      console.log(err);
    });
});

// 404 page
app.use((req, res) => {
  res.status(404).render("404", { title: "404" });
});
